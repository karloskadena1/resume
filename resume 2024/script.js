// Obtener todos los enlaces y pestañas
const all_links = Array.from(document.getElementsByClassName("tab"));
const all_tabs = Array.from(document.getElementsByClassName("tab-content"));

// Agregar un solo evento clic al contenedor de pestañas
all_links.forEach((link) => {
  link.addEventListener("click", function (event) {
    // Verificar si el clic fue en un enlace
    const clicked_link_id = event.target.id;

    // Agregar la clase 'active' al enlace clicado y a los demás enlaces elimina
    all_links.forEach((link) => {
      link.classList.toggle("active", link.id === clicked_link_id);
    });

    // Agregar la clase '__active' a las pestaña seleccionada y eliminarla a las demás si la tienen
    all_tabs.forEach((tab) => {
      const tab_id = tab.id;
      tab.classList.toggle("__active", tab_id.includes(clicked_link_id));
    });
  });
})


